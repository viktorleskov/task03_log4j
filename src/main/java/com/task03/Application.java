package com.task03;

import com.task03.sms.SmsTests;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger LOG = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        LOG.trace("trace");
        LOG.debug("debug");
        LOG.info("info");
        LOG.warn("warn");
        LOG.error("error");
        LOG.fatal("fatal");
        try {
            SmsTests.send("Hi dear");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
