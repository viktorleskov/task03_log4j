package com.task03.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import static com.task03.Consts.*;

public class SmsTests {
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber(USER_PHONE_NUMBER),
                        new PhoneNumber(TWILLIO_PHONE_NUMBER), str).create();
    }
}
